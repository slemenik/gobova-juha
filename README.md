## Gobova juha ##
Projekt pri predmetu Računalniška grafika in tehnologija iger.

![Alt text](https://bytebucket.org/masavinter/gobova-juha/raw/2e2206ce7cb8380b598c603b369e81559b6344f1/screenshot.png?token=86bd601e92ec12d7fa18c12fd2766b13183de923)

### Avtorja ###
* Matej Slemenik
* Maša Vinter

### Opis igre ###

Igra je tretjeosebna pustolovščina, postavljena v neskončen gozd. Igralec nastopa v vlogi nabiralca gob, ki je velik ljubitelj gobove juhe in si zada cilj skuhati najboljšo gobovo juho na svetu. Njegov cilj je v omejenem času nabrati čim večjo količino in čim več različnih vrst užitnih ter čim manj neužitnih gob.