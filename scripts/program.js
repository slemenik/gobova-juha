// Global variable definitionvar canvas
var canvas;
var gl;
var shaderProgram;
var stkock = 200;
var stgob = 200;
var nabranegobe;
var strupenegobe;
var opis;

// Buffers
// World
var worldVertexPositionBuffer;
var worldVertexNormalBuffer;
var worldVertexTextureCoordBuffer;

// Cube
var cubeVertexPositionBuffer;
var cubeVertexTextureCoordBuffer;
var cubeVertexNormalBuffer;
var cubeVertexIndexBuffer;

// Sphere
var sphereVertexPositionBuffer;
var sphereVertexNormalBuffer;
var sphereVertexTextureCoordBuffer;
var sphereVertexIndexBuffer;

// Model-view and projection matrix and model-view matrix stack
var mvMatrixStack;
var mvMatrix;
var pMatrix;

// Variables for storing textures
var floorTexture;
var cubeTexture;
var sphereTexture;

// Variable that stores  loading state of textures.
var texturesLoaded;

// Keyboard handling helper variable for reading the status of keys
var currentlyPressedKeys;

// Variables for storing current position and speed
var yaw;
var yawRate;
var xPosition;
var yPosition;
var zPosition;
var speed;

//spremenljivke za osvetlitev/temnitev
var alfa;
var stopnjaTemnitve;
var stMinut;
var stSekund;

// Helper variable for animation
var lastTime;

// Generira 1000 random števil
var randM;

// Podatki za collision detection
var player;
var kocke;
var gobe;
var vidnostgob;
var kugla;

var pticjaPerspektiva = false;

function inicializacijaSpremenljivk(){
	stopnjaTemnitve = 0.8; //pove koliko naj bo scena osvetljena 0-1
	alfa = 90;
	stMinut = 2;
	stSekund = 0;
	nabranegobe = {"purple":0,"blue":0};
	worldVertexPositionBuffer = null;
	worldVertexTextureCoordBuffer = null;
	mvMatrixStack = [];
	mvMatrix = mat4.create();
	pMatrix = mat4.create();
	texturesLoaded = false;
	currentlyPressedKeys = {};
	yaw = 0;
	yawRate = 0;
	xPosition = 0;
	yPosition = 0.4;
	zPosition = 0;
	speed = 0;
	lastTime = 0;
	randM = [];
	for (var i = 1000; i >= 0; i--) {
		randM.push(Math.random());
	}
	kocke = [];
	gobe = [];
	vidnostgob = [];
	for (var i = stgob - 1; i >= 0; i--) {
		vidnostgob.push(true);
	}
	barvagob = [];
	for (var i = stgob - 1; i >= 0; i--) {
		if (randM[i] < 0.5) {
			barvagob.push("purple");
		} else {
			barvagob.push("blue");
		}	
	}
	pticjaPerspektiva = false;
	document.getElementById("purple").innerHTML = 0;
	document.getElementById("blue").innerHTML = 0;
	document.getElementById("sekunde").innerHTML = "00";
	document.getElementById("minute").innerHTML = 2;
	document.getElementById("music").src = "https://www.youtube.com/embed/nQsay5mL1n4?autoplay=1&start=5";

}

function pobralGobo(barva) {
	nabranegobe[barva] += 1;
	document.getElementById(barva).innerHTML = nabranegobe[barva];
}

function sestejTocke() {
	var sestevek = 0;
	var kljuci = Object.keys(nabranegobe);
	for (var i = kljuci.length - 1; i >= 0; i--) {
		if (strupenegobe[kljuci[i]]) {
			sestevek -= nabranegobe[kljuci[i]]*5;
		} else {
			sestevek += nabranegobe[kljuci[i]]*10;
		}
	}
	if (sestevek <= 0) {
		opis = "neužitna";
	}
	else if (sestevek > 0 && sestevek <= 50) {
		opis = "užitna";
	}
	else if (sestevek > 50 && sestevek <= 100) {
		opis = "dobra";
	}
	else if (sestevek > 100 && sestevek <= 200) {
		opis = "zelo dobra";
	} else {
		opis = "fenomenalna";
	}
	return sestevek;
}

function dolociStrupene() {
	var strupenost = shuffle([true,false]);
	strupenegobe = {"purple":strupenost[0],"blue":strupenost[1]};
	document.getElementById("barva").innerHTML = strupenegobe["purple"] == true ? "vijolične" : "modre";
}

function shuffle(array) {
	var currentIndex = array.length, temporaryValue, randomIndex;
	while (0 !== currentIndex) {
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}
	return array;
}

function izracunajfaktorTemnitve() {
	stMilisekund = (stSekund + stMinut*60) * 1000;
	steviloAnimacij = stMilisekund/15;
	faktor = 0.8/steviloAnimacij;
	return faktor;
}

function izracunajSprememboKota(){
	stMilisekund = (stSekund + stMinut*60) * 1000;
	steviloAnimacij = stMilisekund/15;
	return (0.8/steviloAnimacij)*90;
}

function detectCollision() {
	for (var i = stkock - 1; i >= 0; i--) {
		var distance = Math.sqrt((kocke[i].x - player.x) * (kocke[i].x - player.x) +
                           		 (kocke[i].y - player.y) * (kocke[i].y - player.y) +
                           		 (kocke[i].z - player.z) * (kocke[i].z - player.z));
		if (distance <= (0.5+1.4)) {
			return true;
		}
	}
	for (var i = stgob - 1; i >= 0; i--) {
		var distance = Math.sqrt((gobe[i].x - player.x) * (gobe[i].x - player.x) +
                           		 (gobe[i].y - player.y) * (gobe[i].y - player.y) +
                           		 (gobe[i].z - player.z) * (gobe[i].z - player.z));
		if (distance <= (0.5+0.1)) {
			vidnostgob[i] = false;
			pobralGobo(barvagob[i]);
			gobe[i]={x:-10.0,y:-10.0,z:-10.0};
			return true;
		}
	}
	var distance = Math.sqrt((kugla.x - player.x) * (kugla.x - player.x) +
                           	 (kugla.y - player.y) * (kugla.y - player.y) +
                           	 (kugla.z - player.z) * (kugla.z - player.z));
	if (distance <= (0.5+0.6)) {
		document.getElementById("dialog").style.display="inline";
		return true;
	} else {
		document.getElementById("dialog").style.display="none";
	}
	return false;
}

function lokacijeSphere(mv) {
	var out1 = vec4.create();
	out1 = vec4.transformMat4(out1,vec4.fromValues(0,0,0,1),mv);
	return {x:out1[0],y:out1[1],z:out1[2]};
}

// function lokacijeMinMax(mv) {
// 	var out1 = vec4.create();
// 	var out2 = vec4.create();
// 	out1 = vec4.transformMat4(out1,vec4.fromValues(1,1,1,1),mv);
// 	out2 = vec4.transformMat4(out2,vec4.fromValues(-1,-1,-1,1),mv);
// 	return {xMin:out2[0],xMax:out1[0],yMin:out2[1],yMax:out1[1],zMin:out2[2],zMax:out1[2]};
// }


//
// Matrix utility functions
//
// mvPush   ... push current matrix on matrix stack
// mvPop    ... pop top matrix from stack
// degToRad ... convert degrees to radians
//
function mvPushMatrix() {
	var copy = mat4.clone(mvMatrix);
	//mat4.set(mvMatrix, copy);
	mvMatrixStack.push(copy);
}

function mvPopMatrix() {
	if (mvMatrixStack.length == 0) {
		throw "Invalid popMatrix!";
	}
	mvMatrix = mvMatrixStack.pop();
}

function degToRad(degrees) {
	return degrees * Math.PI / 180;
}

//
// initGL
//
// Initialize WebGL, returning the GL context or null if
// WebGL isn't available or could not be initialized.
//
function initGL(canvas) {
	var gl = null;
	try {
    // Try to grab the standard context. If it fails, fallback to experimental.
    gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
	} catch(e) {}

	// If we don't have a GL context, give up now
	if (!gl) {
		alert("Unable to initialize WebGL. Your browser may not support it.");
	}
	return gl;
}

//
// getShader
//
// Loads a shader program by scouring the current document,
// looking for a script with the specified ID.
//
function getShader(gl, id) {
	var shaderScript = document.getElementById(id);

	// Didn't find an element with the specified ID; abort.
	if (!shaderScript) {
		return null;
	}

	// Walk through the source element's children, building the
	// shader source string.
	var shaderSource = "";
	var currentChild = shaderScript.firstChild;
	while (currentChild) {
		if (currentChild.nodeType == 3) {
			shaderSource += currentChild.textContent;
		}
		currentChild = currentChild.nextSibling;
	}

	// Now figure out what type of shader script we have,
	// based on its MIME type.
	var shader;
	if (shaderScript.type == "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (shaderScript.type == "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
	return null;  // Unknown shader type
}

	// Send the source to the shader object
	gl.shaderSource(shader, shaderSource);

	// Compile the shader program
	gl.compileShader(shader);

	// See if it compiled successfully
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
		return null;
	}
	return shader;
}

//
// initShaders
//
// Initialize the shaders, so WebGL knows how to light our scene.
//
function initShaders() {
	var fragmentShader = getShader(gl, "shader-fs");
	var vertexShader = getShader(gl, "shader-vs");

	// Create the shader program
	shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);

	// If creating the shader program failed, alert
	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert("Unable to initialize the shader program.");
	}

	// start using shading program for rendering
	gl.useProgram(shaderProgram);

	shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
	gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

	shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
  	gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

	shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
	gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

	shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
	shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
	shaderProgram.nMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
	shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");

	shaderProgram.useLightingUniform = gl.getUniformLocation(shaderProgram, "uUseLighting");
  	shaderProgram.ambientColorUniform = gl.getUniformLocation(shaderProgram, "uAmbientColor");
  	shaderProgram.lightingDirectionUniform = gl.getUniformLocation(shaderProgram, "uLightingDirection");
  	shaderProgram.directionalColorUniform = gl.getUniformLocation(shaderProgram, "uDirectionalColor");
}

//
// setMatrixUniforms
//
// Set the uniforms in shaders.
//
function setMatrixUniforms() {
	gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
	gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);

	var normalMatrix = mat3.create();  	
  	normalMatrix = mat3.normalFromMat4(normalMatrix, mvMatrix);
  	gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, normalMatrix);
}

//
// initTextures
//
// Initialize the textures we'll be using, then initiate a load of
// the texture images. The handleTextureLoaded() callback will finish
// the job; it gets called each time a texture finishes loading.
//
function initTextures() {
	// Floor
	floorTexture = gl.createTexture();
	floorTexture.image = new Image();
	floorTexture.image.onload = function () {
		handleTextureLoaded(floorTexture)
	}
	floorTexture.image.src = "./assets/floor.png";

	// Cube
	cubeTexture = gl.createTexture();
	cubeTexture.image = new Image();
	cubeTexture.image.onload = function () {
		handleTextureLoaded(cubeTexture)
	}
	cubeTexture.image.src = "./assets/tree.png";

	// Mushrooms
	blueTexture = gl.createTexture();
	blueTexture.image = new Image();
	blueTexture.image.onload = function () {
		handleTextureLoaded(blueTexture)
	}
	blueTexture.image.src = "./assets/blue.png";

	purpleTexture = gl.createTexture();
	purpleTexture.image = new Image();
	purpleTexture.image.onload = function () {
		handleTextureLoaded(purpleTexture)
	}
	purpleTexture.image.src = "./assets/purple.png";

	// Player
	basketTexture = gl.createTexture();
	basketTexture.image = new Image();
	basketTexture.image.onload = function () {
		handleTextureLoaded(basketTexture)
	}
	basketTexture.image.src = "./assets/basket.png";

	// Sphere
	sphereTexture = gl.createTexture();
  	sphereTexture.image = new Image();
  	sphereTexture.image.onload = function () {
    	handleTextureLoaded(sphereTexture)
  	}
  	sphereTexture.image.src = "./assets/head.png";

}

function handleTextureLoaded(texture) {
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.generateMipmap(gl.TEXTURE_2D);

	gl.bindTexture(gl.TEXTURE_2D, null);

	// when texture loading is finished we can draw scene.
	texturesLoaded = true;
}

//
// handleLoadedWorld
//
// Initialisation of world 
//
function loadWorld() {
	a = 300.0
	b = 300.0
	var vertexPositions = [
		-a, 0.0, -a,
		-a, 0.0, a,
		a, 0.0, a,
		-a, 0.0, -a,
		a, 0.0, -a,
		a, 0.0, a
	];

	var vertexNormals = [
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0
  ];

	var vertexTextureCoords = [
		0.0, 2*b,
		0.0, 0.0,
		2*b, 0.0,
		0.0, 2*b,
		2*b, 2*b,
		2*b, 0.0
	];

	worldVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexPositions), gl.STATIC_DRAW);
	worldVertexPositionBuffer.itemSize = 3;
	worldVertexPositionBuffer.numItems = 6;

	worldVertexNormalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexNormalBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);
	worldVertexNormalBuffer.itemSize = 3;
	worldVertexNormalBuffer.numItems = 6;

	worldVertexTextureCoordBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexTextureCoordBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexTextureCoords), gl.STATIC_DRAW);
	worldVertexTextureCoordBuffer.itemSize = 2;
	worldVertexTextureCoordBuffer.numItems = 6;

}

//
// initBuffers
//
// Initialize the buffers we'll need. For this demo, we just have
// one object -- a simple cube.
//
function initBuffers() {
	// Create a buffer for the cube's vertices.
	cubeVertexPositionBuffer = gl.createBuffer();

	// Select the cubeVertexPositionBuffer as the one to apply vertex
	// operations to from here out.
	gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);

	// Now create an array of vertices for the cube.
	vertices = [
    // Front face
    -1.0, -1.0,  1.0,
    1.0, -1.0,  1.0,
    1.0,  1.0,  1.0,
    -1.0,  1.0,  1.0,

    // Back face
    -1.0, -1.0, -1.0,
    -1.0,  1.0, -1.0,
    1.0,  1.0, -1.0,
    1.0, -1.0, -1.0,

    // Top face
    -1.0,  1.0, -1.0,
    -1.0,  1.0,  1.0,
    1.0,  1.0,  1.0,
    1.0,  1.0, -1.0,

    // Bottom face
    -1.0, -1.0, -1.0,
    1.0, -1.0, -1.0,
    1.0, -1.0,  1.0,
    -1.0, -1.0,  1.0,

    // Right face
    1.0, -1.0, -1.0,
    1.0,  1.0, -1.0,
    1.0,  1.0,  1.0,
    1.0, -1.0,  1.0,

    // Left face
    -1.0, -1.0, -1.0,
    -1.0, -1.0,  1.0,
    -1.0,  1.0,  1.0,
    -1.0,  1.0, -1.0
    ];

	// Now pass the list of vertices into WebGL to build the shape. We
	// do this by creating a Float32Array from the JavaScript array,
	// then use it to fill the current vertex buffer.
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
	cubeVertexPositionBuffer.itemSize = 3;
	cubeVertexPositionBuffer.numItems = 24;

	cubeVertexNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexNormalBuffer);

  // Now create an array of vertex normals for the cube.
  var vertexNormals = [
      // Front face
       0.0,  0.0,  1.0,
       0.0,  0.0,  1.0,
       0.0,  0.0,  1.0,
       0.0,  0.0,  1.0,

      // Back face
       0.0,  0.0, -1.0,
       0.0,  0.0, -1.0,
       0.0,  0.0, -1.0,
       0.0,  0.0, -1.0,

      // Top face
       0.0,  1.0,  0.0,
       0.0,  1.0,  0.0,
       0.0,  1.0,  0.0,
       0.0,  1.0,  0.0,

      // Bottom face
       0.0, -1.0,  0.0,
       0.0, -1.0,  0.0,
       0.0, -1.0,  0.0,
       0.0, -1.0,  0.0,

      // Right face
       1.0,  0.0,  0.0,
       1.0,  0.0,  0.0,
       1.0,  0.0,  0.0,
       1.0,  0.0,  0.0,

      // Left face
      -1.0,  0.0,  0.0,
      -1.0,  0.0,  0.0,
      -1.0,  0.0,  0.0,
      -1.0,  0.0,  0.0
  ];

  // Pass the normals into WebGL
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);
  cubeVertexNormalBuffer.itemSize = 3;
  cubeVertexNormalBuffer.numItems = 24;

	// Map the texture onto the cube's faces.
	cubeVertexTextureCoordBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);

	// Now create an array of vertex texture coordinates for the cube.
	var textureCoordinates = [
    // Front
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0,
    // Back
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0,
    // Top
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0,
    // Bottom
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0,
    // Right
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0,
    // Left
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0
    ];

	// Pass the texture coordinates into WebGL
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates), gl.STATIC_DRAW);
	cubeVertexTextureCoordBuffer.itemSize = 2;
	cubeVertexTextureCoordBuffer.numItems = 24;

	// Build the element array buffer; this specifies the indices
	// into the vertex array for each face's vertices.
	cubeVertexIndexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);

	// This array defines each face as two triangles, using the
	// indices into the vertex array to specify each triangle's
	// position.
	var cubeVertexIndices = [
    0,  1,  2,      0,  2,  3,    // front
    4,  5,  6,      4,  6,  7,    // back
    8,  9,  10,     8,  10, 11,   // top
    12, 13, 14,     12, 14, 15,   // bottom
    16, 17, 18,     16, 18, 19,   // right
    20, 21, 22,     20, 22, 23    // left
    ];

	// Now send the element array to GL
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
	cubeVertexIndexBuffer.itemSize = 1;
	cubeVertexIndexBuffer.numItems = 36;

	/////////////////////////////////////  KROGLA  ///////////////////////////////////////

	var latitudeBands = 30;
    var longitudeBands = 30;
    var radius = 2;

	var vertexPositionData = [];
	var normalData = [];
	var textureCoordData = [];
	for (var latNumber=0; latNumber <= latitudeBands; latNumber++) {
		var theta = latNumber * Math.PI / latitudeBands;
		var sinTheta = Math.sin(theta);
		var cosTheta = Math.cos(theta);

		for (var longNumber=0; longNumber <= longitudeBands; longNumber++) {
		  var phi = longNumber * 2 * Math.PI / longitudeBands;
		  var sinPhi = Math.sin(phi);
		  var cosPhi = Math.cos(phi);

		  var x = cosPhi * sinTheta;
		  var y = cosTheta;
		  var z = sinPhi * sinTheta;
		  var u = 1 - (longNumber / longitudeBands);
		  var v = 1 - (latNumber / latitudeBands);

		  normalData.push(x);
		  normalData.push(y);
		  normalData.push(z);
		  textureCoordData.push(u);
		  textureCoordData.push(v);
		  vertexPositionData.push(radius * x);
		  vertexPositionData.push(radius * y);
		  vertexPositionData.push(radius * z);
		}
	}

	var indexData = [];
	for (var latNumber=0; latNumber < latitudeBands; latNumber++) {
		for (var longNumber=0; longNumber < longitudeBands; longNumber++) {
		  var first = (latNumber * (longitudeBands + 1)) + longNumber;
		  var second = first + longitudeBands + 1;
		  indexData.push(first);
		  indexData.push(second);
		  indexData.push(first + 1);

		  indexData.push(second);
		  indexData.push(second + 1);
		  indexData.push(first + 1);
	    }
  	}

  	// Pass the normals into WebGL
	sphereVertexNormalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexNormalBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normalData), gl.STATIC_DRAW);
	sphereVertexNormalBuffer.itemSize = 3;
	sphereVertexNormalBuffer.numItems = normalData.length / 3;

	// Pass the texture coordinates into WebGL
	sphereVertexTextureCoordBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexTextureCoordBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordData), gl.STATIC_DRAW);
	sphereVertexTextureCoordBuffer.itemSize = 2;
	sphereVertexTextureCoordBuffer.numItems = textureCoordData.length / 2;

	// Pass the vertex positions into WebGL
	sphereVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexPositionData), gl.STATIC_DRAW);
	sphereVertexPositionBuffer.itemSize = 3;
	sphereVertexPositionBuffer.numItems = vertexPositionData.length / 3;

	// Pass the indices into WebGL
	sphereVertexIndexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, sphereVertexIndexBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexData), gl.STREAM_DRAW);
	sphereVertexIndexBuffer.itemSize = 1;
	sphereVertexIndexBuffer.numItems = indexData.length;
}

//
// drawScene
//
// Draw the scene.
//
function drawScene() {
	// set the rendering environment to full canvas size
	gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
	// Clear the canvas before we start drawing on it.
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// If buffers are empty we stop loading the application.
	if (worldVertexTextureCoordBuffer == null || worldVertexPositionBuffer == null) {
		return;
	}

	// Establish the perspective with which we want to view the
	// scene. Our field of view is 45 degrees, with a width/height
	// ratio of 640:480, and we only want to see objects between 0.1 units
	// and 100 units away from the camera.
	mat4.perspective(pMatrix, 45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0);

	// Set the drawing position to the "identity" point, which is
	// the center of the scene.
	mat4.identity(mvMatrix);
	mat4.translate(mvMatrix, mvMatrix, vec3.fromValues(0, -1.0, -3.0));
	if (pticjaPerspektiva) {
		mat4.rotate(mvMatrix, mvMatrix, degToRad(90), vec3.fromValues(1, 0, 0));
		mat4.translate(mvMatrix, mvMatrix, vec3.fromValues(0, -5, 0));
	}
	

	
	/////////////////////////////////////////////////////////////////

	// Player
	// Now move the drawing position a bit to where we want to start
	// drawing the player.

	mvPushMatrix();
	mat4.translate(mvMatrix, mvMatrix, vec3.fromValues(0, 0.0, -1.0));
	mat4.scale(mvMatrix, mvMatrix, vec3.fromValues(0.5,0.5,0.5));

	// Draw the cube by binding the array buffer to the cube's vertices
	// array, setting attributes, and pushing it to GL.
	gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

	gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexNormalBuffer);
  	gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, cubeVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

	// Set the texture coordinates attribute for the vertices.
	gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
	gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, cubeVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

	// Specify the texture to map onto the faces.
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, basketTexture);
	gl.uniform1i(shaderProgram.samplerUniform, 0);

	// Draw the cube.
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);
	setMatrixUniforms();
	gl.drawElements(gl.TRIANGLES, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
	player = lokacijeSphere(mvMatrix);
	mvPopMatrix();

	/////////////////////////////////////////////////////////////////

	// Cube
	// Now move the drawing position a bit to where we want to start
	// drawing the cube.

	for (var i = stkock - 1; i >= 0; i--) {
		j = randM[i]*7;
		k = randM[i+1]*7;
		mvPushMatrix();
		mat4.rotate(mvMatrix, mvMatrix, degToRad(-yaw), vec3.fromValues(0, 1, 0));
		mat4.translate(mvMatrix, mvMatrix, vec3.fromValues(-xPosition, -yPosition, -zPosition));
		mat4.translate(mvMatrix, mvMatrix, vec3.fromValues((-1)^j*5.0*j, 1.0, (-1)^k*5.0*k+10));

		// Draw the cube by binding the array buffer to the cube's vertices
		// array, setting attributes, and pushing it to GL.
		gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

		gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexNormalBuffer);
  		gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, cubeVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Set the texture coordinates attribute for the vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
		gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, cubeVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Specify the texture to map onto the faces.
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
		gl.uniform1i(shaderProgram.samplerUniform, 0);

		// Draw the cube.
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);
		setMatrixUniforms();
		gl.drawElements(gl.TRIANGLES, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
		kocke[i]=lokacijeSphere(mvMatrix);

	/////////////////////////////////////////////////////////////////

	// Gobe

		if (i <= stgob && vidnostgob[i]) {
			mat4.translate(mvMatrix, mvMatrix, vec3.fromValues(-2.0, -0.9, 1.0));
			mat4.scale(mvMatrix, mvMatrix, vec3.fromValues(0.1,0.1,0.1));

			gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
			gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

			// Set the texture coordinates attribute for the vertices.
			gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
			gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, cubeVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

			// Specify the texture to map onto the faces.
			if (barvagob[i] == "purple") {
				gl.activeTexture(gl.TEXTURE0);
				gl.bindTexture(gl.TEXTURE_2D, purpleTexture);
				gl.uniform1i(shaderProgram.samplerUniform, 0);
			} else {
				gl.activeTexture(gl.TEXTURE0);
				gl.bindTexture(gl.TEXTURE_2D, blueTexture);
				gl.uniform1i(shaderProgram.samplerUniform, 0);
			}	
			
			// Draw the cube.
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);
			setMatrixUniforms();
			gl.drawElements(gl.TRIANGLES, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
			gobe[i]=lokacijeSphere(mvMatrix);
		}
		

	/////////////////////////////////////////////////////////////////

		mvPopMatrix();
	}

	///////////////////////  SPHERE ////////////////////////

	mvPushMatrix();

	// Now move the drawing position a bit to where we want to start
	// drawing.

	mat4.rotate(mvMatrix, mvMatrix, degToRad(-yaw), vec3.fromValues(0, 1, 0));
	mat4.translate(mvMatrix, mvMatrix, vec3.fromValues(-xPosition, -yPosition, -zPosition));
	mat4.translate(mvMatrix, mvMatrix, vec3.fromValues(5, 0.6, -5.0));
	mat4.scale(mvMatrix, mvMatrix, vec3.fromValues(0.3,0.3,0.3));
	
	mat4.rotate(mvMatrix, mvMatrix, degToRad(yaw+90), vec3.fromValues(0, 1, 0));//obraz vedno gleda proti igralcu
	mat4.rotate(mvMatrix, mvMatrix, degToRad(-20), vec3.fromValues(0, 0, 1));//dvig obraza naravnost

	// Activate textures
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, sphereTexture);
	gl.uniform1i(shaderProgram.samplerUniform, 0);

	// Set the vertex positions attribute for the sphere vertices.
	gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexPositionBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, sphereVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

	// Set the texture coordinates attribute for the vertices.
	gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexTextureCoordBuffer);
	gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, sphereVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

	// Set the normals attribute for the vertices.
	gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexNormalBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, sphereVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

	// Draw the sphere
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, sphereVertexIndexBuffer);
	setMatrixUniforms();
	gl.drawElements(gl.TRIANGLES, sphereVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
	kugla = lokacijeSphere(mvMatrix);

	// restore last location
	mvPopMatrix();
	
	/////////////////////////////////////////////////////////////////


	// Floor
	// Now move the drawing position a bit to where we want to start
	// drawing the world.

	mvPushMatrix();
	mat4.rotate(mvMatrix, mvMatrix, degToRad(-yaw), vec3.fromValues(0, 1, 0));
	mat4.translate(mvMatrix, mvMatrix, vec3.fromValues(-xPosition, -yPosition, -zPosition));

	// Activate textures
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, floorTexture);
	gl.uniform1i(shaderProgram.samplerUniform, 0);

	// Set the texture coordinates attribute for the vertices.
	gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexTextureCoordBuffer);
	gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, worldVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

	gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexNormalBuffer);
  	gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, worldVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

	// Draw the world by binding the array buffer to the world's vertices
	// array, setting attributes, and pushing it to GL.
	gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexPositionBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, worldVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

	// Draw the cube.
	setMatrixUniforms();
	gl.drawArrays(gl.TRIANGLES, 0, worldVertexPositionBuffer.numItems);
	mvPopMatrix();

	////////////////////////////////////////////////////////SVETLOBA//////////////////////////////////////
	gl.uniform1i(shaderProgram.useLightingUniform, true);
	gl.uniform3f(shaderProgram.ambientColorUniform, stopnjaTemnitve, stopnjaTemnitve, stopnjaTemnitve);
	var lightingDirection = [-Math.cos(degToRad(alfa)), -Math.sin(degToRad(alfa)),	Math.cos(degToRad(yaw))];//simulacija sonca

	var adjustedLD = vec3.create();
	vec3.normalize(adjustedLD, lightingDirection);
	vec3.scale(adjustedLD, adjustedLD, -1);
	gl.uniform3fv(shaderProgram.lightingDirectionUniform, adjustedLD);

	gl.uniform3f(shaderProgram.directionalColorUniform,0.9,0.7,0.7);//bolj rdečkasta svetloba


	/////////////////////////////////////////////////////////////////

}

//
// animate
//
// Called every time before redeawing the screen.
//
function animate() {
	var timeNow = new Date().getTime();
	if (lastTime != 0) {
		var elapsed = timeNow - lastTime;

		if (speed != 0) {
			tempSpeed = speed;
if (player && detectCollision() ) speed = 0;
			xPosition -= Math.sin(degToRad(yaw)) * speed * elapsed;
			
			zPosition -= Math.cos(degToRad(yaw)) * speed * elapsed;
		}
	yaw += yawRate * elapsed;
	}
	lastTime = timeNow;
}

//
// Keyboard handling helper functions
//
// handleKeyDown    ... called on keyDown event
// handleKeyUp      ... called on keyUp event
//
function handleKeyDown(event) {
	// storing the pressed state for individual key
	currentlyPressedKeys[event.keyCode] = true;
}

function handleKeyUp(event) {
	// reseting the pressed state for individual key
	currentlyPressedKeys[event.keyCode] = false;
}

//
// handleKeys
//
// Called every time before redeawing the screen for keyboard
// input handling. Function continuisly updates helper variables.
//
function handleKeys() {
	if (currentlyPressedKeys[37] || currentlyPressedKeys[65]) {
    // Left cursor key or A
    yawRate = 0.1;
	} else if (currentlyPressedKeys[39] || currentlyPressedKeys[68]) {
    // Right cursor key or D
    yawRate = -0.1;
	} else {
	yawRate = 0;
	}
	if (currentlyPressedKeys[38] || currentlyPressedKeys[87]) {
	    // Up cursor key or W
	    speed = 0.008;
	} else if (currentlyPressedKeys[40] || currentlyPressedKeys[83]) {
	    // Down cursor key
	    speed = -0.008;
	} else {
		speed = 0;
	}
	if (currentlyPressedKeys[79] && (pticjaPerspektiva == true)) {
	    pticjaPerspektiva = false;
	} else if (currentlyPressedKeys[80] && (pticjaPerspektiva == false)) {		
		pticjaPerspektiva = true;
	}
}

//
// start
//
// Called when the canvas is created to get the ball rolling.
// Figuratively, that is. There's nothing moving in this demo.
//
function start() {
	inicializacijaSpremenljivk();
	dolociStrupene();
	faktorTemnitve = izracunajfaktorTemnitve();//pove za koliko naj se v vsakem frame-u zniža svetloba
	faktorKota = izracunajSprememboKota();
	
	canvas = document.getElementById("glcanvas");

	gl = initGL(canvas);      // Initialize the GL context

	// Only continue if WebGL is available and working
	if (gl) {
	    gl.clearColor(152/255,150/255,230/255,1);                    // Set clear color to black, fully opaque
	    gl.clearDepth(1.0);                                     // Clear everything
	    gl.enable(gl.DEPTH_TEST);                               // Enable depth testing
	    gl.depthFunc(gl.LEQUAL);                                // Near things obscure far things

	    // Initialize the shaders; this is where all the lighting for the
	    // vertices and so forth is established.
	    initShaders();

	    // Here's where we call the routine that builds all the objects
	    // we'll be drawing.
	    initBuffers();
	    
	    // Next, load and set up the textures we'll be using.
	    initTextures();

	    // Initialise world objects
	    loadWorld();

	    // Bind keyboard handling functions to document handlers
	    document.onkeydown = handleKeyDown;
	    document.onkeyup = handleKeyUp;
	    
	    // Set up to draw the scene periodically.
	    intervalID = setInterval(function() {
			if (texturesLoaded) { // only draw scene and animate when textures are loaded.
				requestAnimationFrame(animate);
				handleKeys();

				stopnjaTemnitve -= faktorTemnitve;
		        alfa += faktorKota;
		        if (stSekund == 0 && stMinut == 0){
		        	clearInterval(intervalID);
		        	clearInterval(intervalTime);
		        	document.getElementById("music").src = "";
		        	var sestevek = sestejTocke();
					var nova = window.confirm("Zbral si " + sestevek + " točk. Tvoja juha je " + opis + ". Začni novo igro?");
					if (nova == true) {
        				start();
        			}
		        }		        
		        drawScene();
			}
		}, 15);

		intervalTime = setInterval(function(){
			stSekund--;
			if (stSekund < 0) {
				stSekund = 59;
				stMinut--;
			}
			if (stSekund < 10) {
				document.getElementById("sekunde").innerHTML = "0"+stSekund;
			} else {
				document.getElementById("sekunde").innerHTML = stSekund;
			}
			document.getElementById("minute").innerHTML = stMinut;
		}, 1000);
	}
}

/*

TODO:
- dodaj navodila v html

- poročilo


*/